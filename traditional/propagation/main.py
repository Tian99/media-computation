
TODO: 
0. Set hyperparameters.
1. Convert all the pixels from RGB space into affinity space.
2. Build the kd-tree according to the affinity coordinate.
3. Generate T-junction and color interpolation from kd-tree.

import cv2 
# Note that cv2 is BGR

if __name__ == "__main__":
    img = cv2.imread("data/trg.jpg")